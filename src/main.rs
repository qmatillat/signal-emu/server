mod curve_generator;
use std::net::Ipv4Addr;

mod error;

mod out;
use out::{Out, TypedWriter};

mod server;
use server::Server;

mod ws;
use ws::WebSocketServer;

#[tokio::main]
async fn main() {
    let srv = Server::default();

    if let Err(err) = srv.load(include_str!("../conf.ron")) {
        println!("[ERROR] Failed to load config: {err}.\nDefault to no register")
    }

    let _ = tokio::join! {
        // start_modus_server(srv.clone()),
        tokio::spawn(srv.clone().run_modbus((Ipv4Addr::UNSPECIFIED, 8000))),
        tokio::spawn(WebSocketServer::new(srv).run((Ipv4Addr::UNSPECIFIED, 8888))),
    };
}
