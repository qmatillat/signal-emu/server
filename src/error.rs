use serde::Serialize;
use thiserror::Error;

#[derive(Debug, Error, Clone, Serialize)]
pub enum Error {
    #[error("Interacting with unconfigured address {addr}")]
    UnconfiguredAddr { addr: u16 },
    #[error("Output too short to fill value at address {addr}")]
    OutputTooShort { addr: u16 },
    #[error("Partial write. Wrote {wrote} but expected to write {expected}")]
    PartialWrite { wrote: u16, expected: u16 },
}
