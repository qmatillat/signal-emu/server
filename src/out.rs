use crate::error::Error;

pub struct Out {
    out: Vec<u16>,
    write_idx: usize,
    addr: u16,
}

impl Out {
    pub fn new(addr: u16, size: u16) -> Self {
        Self {
            out: vec![0; size as usize],
            addr,
            write_idx: 0,
        }
    }

    pub fn can_write(&self) -> bool {
        self.write_idx < self.out.len()
    }

    pub fn next_addr(&self) -> Option<u16> {
        if !self.can_write() {
            return None;
        }

        let pos = self.write_idx.try_into().ok()?;
        self.addr.checked_add(pos)
    }

    pub fn check_finish(&self) -> Result<(), Error> {
        if self.out.len() != self.write_idx {
            return Err(Error::PartialWrite {
                wrote: self.write_idx as u16,
                expected: self.out.len() as u16,
            });
        }

        Ok(())
    }

    pub fn finish(self) -> Vec<u16> {
        self.out
    }
}

pub trait TypedWriter<V> {
    fn write(&mut self, v: V) -> Result<(), Error>;
}

impl TypedWriter<u16> for Out {
    fn write(&mut self, v: u16) -> Result<(), Error> {
        if !self.can_write() {
            return Err(Error::OutputTooShort {
                addr: self.addr + self.write_idx as u16,
            });
        }
        self.out[self.write_idx] = v;
        self.write_idx += 1;

        Ok(())
    }
}

impl TypedWriter<i16> for Out {
    fn write(&mut self, v: i16) -> Result<(), Error> {
        self.write(unsafe { std::mem::transmute::<i16, u16>(v) })?;
        Ok(())
    }
}

impl TypedWriter<i32> for Out {
    fn write(&mut self, v: i32) -> Result<(), Error> {
        let data: u32 = unsafe { std::mem::transmute(v) };
        self.write((data & 0xFFFF) as u16)?;
        self.write((data >> 16) as u16)?;
        Ok(())
    }
}

impl TypedWriter<f32> for Out {
    fn write(&mut self, v: f32) -> Result<(), Error> {
        let data: u32 = v.to_bits();
        self.write((data & 0xFFFF) as u16)?;
        self.write((data >> 16) as u16)?;
        Ok(())
    }
}

impl TypedWriter<f64> for Out {
    fn write(&mut self, v: f64) -> Result<(), Error> {
        let data: u64 = v.to_bits();

        self.write((data & 0xFFFF) as u16)?;
        self.write(((data >> 16) & 0xFFFF) as u16)?;
        self.write(((data >> 32) & 0xFFFF) as u16)?;
        self.write(((data >> 48) & 0xFFFF) as u16)?;
        Ok(())
    }
}
