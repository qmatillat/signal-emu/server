use std::{collections::BTreeMap, time::SystemTime};

use crate::{curve_generator::Value, error::Error};
use serde::Serialize;
use tokio::sync::broadcast;

#[derive(Debug)]
pub struct History {
    hist: broadcast::Sender<(SystemTime, Entry)>,
}

impl Default for History {
    fn default() -> Self {
        Self {
            hist: broadcast::channel(8).0,
        }
    }
}

impl History {
    pub fn push(&self, entry: Entry) {
        let _ = self.hist.send((SystemTime::now(), entry));
    }

    pub fn subscribe(&self) -> Reader {
        Reader(self.hist.subscribe())
    }
}

pub struct Reader(broadcast::Receiver<(SystemTime, Entry)>);

impl Reader {
    pub async fn recv(&mut self) -> Result<(SystemTime, Entry), broadcast::error::RecvError> {
        self.0.recv().await
    }
}

#[derive(Clone, Debug, Serialize)]
pub enum Entry {
    Read { addr: u16, value: Value },
    Poll(BTreeMap<u16, Value>),
    // Write
    Error(Error),
}
