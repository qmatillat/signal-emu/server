use crate::{
    curve_generator::{Context, Register, RegisterType, Value},
    error::Error,
    out::{Out, TypedWriter},
};
use std::{collections::BTreeMap, sync::Arc};
use tokio_modbus::server::NewService;

pub mod history;
use history::History;

mod conf;
use self::conf::Config;
pub use self::conf::{ServerConfig, Watcher as ConfWatcher};

mod modbus_client;
use modbus_client::Client;

#[derive(Debug, Default)]
struct InnerServer {
    config: Config,
    history: History,
}

#[derive(Debug, Default, Clone)]
pub struct Server(Arc<InnerServer>);

impl NewService for Server {
    type Request = tokio_modbus::prelude::Request;
    type Response = tokio_modbus::prelude::Response;
    type Error = std::io::Error;
    type Instance = Client;

    fn new_service(&self) -> std::io::Result<Self::Instance> {
        Ok(Client::new(self.clone()))
    }
}

impl Server {
    pub async fn run_modbus(
        self,
        socket_addr: impl Into<std::net::SocketAddr>,
    ) -> std::io::Result<()> {
        let s = tokio_modbus::server::tcp::Server::new(socket_addr.into());

        s.serve(self).await
    }

    pub fn load(&self, config: &str) -> Result<(), ron::Error> {
        let new_conf = ron::from_str(config)?;
        self.0.config.update(move |conf| *conf = new_conf);
        Ok(())
    }

    fn report_error(&self, err: Error) {
        println!("[ERROR] {err}");
        self.0.history.push(history::Entry::Error(err));
    }

    fn get_regs(&self, addr: u16, size: u16) -> Vec<u16> {
        let time = std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_secs_f64();

        let config = self.0.config.read();
        let mut out = Out::new(addr, size);
        while let Some(addr) = out.next_addr() {
            let value = config.slots.get(&addr).map_or_else(
                || {
                    self.report_error(Error::UnconfiguredAddr { addr });
                    Value::default()
                },
                |r| r.read(&Context { time, reg_id: addr }),
            );
            self.0.history.push(history::Entry::Read { addr, value });
            if let Err(err) = out.write(value) {
                self.report_error(err)
            }
        }

        if let Err(err) = out.check_finish() {
            self.report_error(err)
        }

        out.finish()
    }

    fn snapshot(&self) -> BTreeMap<u16, Value> {
        let time = std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_secs_f64();

        let config = self.0.config.read();
        config
            .slots
            .iter()
            .map(|(&reg_id, r)| (reg_id, r.read(&Context { time, reg_id })))
            .collect()
    }

    pub fn send_snapshot(&self) {
        self.0.history.push(history::Entry::Poll(self.snapshot()))
    }

    pub fn watch(&self) -> (ConfWatcher, history::Reader) {
        (self.0.config.subscribe(), self.0.history.subscribe())
    }

    pub fn set_register_conf(&self, reg_id: u16, reg_conf: Option<RegisterType>) {
        self.0.config.update(|conf| match reg_conf {
            Some(reg_conf) => conf.slots.insert(reg_id, reg_conf),
            None => conf.slots.remove(&reg_id),
        });
    }

    pub fn set_poll_interval(&self, duration: Option<std::time::Duration>) {
        let duration = duration.and_then(|e| if e.is_zero() { None } else { Some(e) });

        self.0.config.update(|conf| conf.poll_interval = duration);
    }

    pub fn config(&self) -> ServerConfig {
        self.0.config.read().clone()
    }
}
