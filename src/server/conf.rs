use crate::curve_generator::RegisterType;
use serde::{Deserialize, Serialize};
use std::{collections::BTreeMap, ops::Deref, sync::RwLock};
use tokio::sync::watch;

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct ServerConfig {
    pub slots: BTreeMap<u16, RegisterType>,

    pub poll_interval: Option<std::time::Duration>,
}

#[derive(Debug)]
pub struct Config {
    config: RwLock<ServerConfig>,
    conf_update: watch::Sender<()>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            config: Default::default(),
            conf_update: watch::channel(()).0,
        }
    }
}

impl Config {
    pub fn update<F: FnOnce(&mut ServerConfig) -> Out, Out>(&self, f: F) -> Out {
        let o = f(&mut self.config.write().unwrap());

        // Ignore if no one is listening
        let _ = self.conf_update.send(());

        o
    }

    pub fn read(&self) -> impl Deref<Target = ServerConfig> + '_ {
        self.config.read().unwrap()
    }

    pub fn subscribe(&self) -> Watcher {
        Watcher(self.conf_update.subscribe())
    }
}

pub struct Watcher(watch::Receiver<()>);

impl Watcher {
    pub async fn changed(&mut self) {
        if self.0.changed().await.is_err() {
            // Sender should still be here
            unreachable!()
        }
    }
}
