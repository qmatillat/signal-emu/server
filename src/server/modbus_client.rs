use std::future;

use tokio_modbus::{prelude::Response, server::Service};

use crate::server::Server;

pub struct Client(Server);

impl Service for Client {
    type Request = tokio_modbus::prelude::Request;
    type Response = tokio_modbus::prelude::Response;
    type Error = std::io::Error;
    type Future = std::future::Ready<Result<Self::Response, Self::Error>>;

    fn call(&self, req: Self::Request) -> Self::Future {
        future::ready(Ok(self.call(req)))
    }
}

impl Client {
    pub fn new(server: Server) -> Self {
        Self(server)
    }

    fn call(&self, req: tokio_modbus::prelude::Request) -> tokio_modbus::prelude::Response {
        match req {
            tokio_modbus::prelude::Request::ReadInputRegisters(addr, size) => {
                Response::ReadInputRegisters(self.0.get_regs(addr, size))
            }
            tokio_modbus::prelude::Request::ReadHoldingRegisters(addr, size) => {
                Response::ReadHoldingRegisters(self.0.get_regs(addr, size))
            }
            _ => unimplemented!(),
        }
    }
}
