use super::{Out, TypedWriter};
use enum_dispatch::enum_dispatch;
use noise::{NoiseFn, OpenSimplex};
use serde::{Deserialize, Serialize};
use std::ops::Range;

pub struct Context {
    pub time: f64,
    pub reg_id: u16,
}

#[enum_dispatch]
pub trait Register {
    fn read(&self, ctx: &Context) -> Value;

    #[must_use]
    fn write(&mut self, _val: Value) -> bool {
        false
    }
}

macro_rules! gen_value_type {
    ( gen_into $( $name:ident ),* => ) => {};
    ( gen_into $( $name:ident ),* => $ty:ty, $( $tys:ty, )* ) => {
        impl From<Value> for $ty {
            fn from(v: Value) -> Self {
                match v {
                   $( Value::$name(v) => v as _ ),*
                }
            }
        }

        gen_value_type!{ gen_into $( $name ),* => $( $tys, )* }
    };

    ( gen_math $( $name:ident ),* for ) => {};
    ( gen_math $( $name:ident ),* for $trait:ident $fn:ident $( ,$traits:ident $fns:ident )* ) => {
        impl std::ops::$trait for Value {
            type Output = Self;
            fn $fn(self, rhs: Self) -> Self {
                let ty = self.ty().and(&rhs.ty());
                let sel = self.cast(ty);
                let rhs = rhs.cast(ty);

                match (sel, rhs) {
                    $(
                    (Self::$name(sel), Self::$name(rhs)) => Self::$name(sel.$fn(rhs)),
                    )*

                    _ => unreachable!(),
                }
            }
        }
        gen_value_type!{ gen_math $( $name ),* for $( $traits $fns ),* }
    };

    ( $( $name:ident => $ty:ty),* ) => {
        #[derive(Debug, Clone, Copy, Serialize, Deserialize)]
        pub enum Type {
            $($name),*
        }

        #[derive(Debug, Clone, Copy, Serialize, Deserialize)]
        pub enum Value {
            $($name($ty)),*
        }

        $(
        impl From<$ty> for Value {
            fn from(v: $ty) -> Self {
                Self::$name(v)
            }
        }
        )*

        gen_value_type!{ gen_into $( $name ),* => $( $ty, )* }

        impl Value {
            fn ty(&self) -> Type {
                match self {
                    $( Self::$name(_) => Type::$name ),*
                }
            }
            fn cast(self, ty: Type) -> Self {
                match ty {
                    $( Type::$name => Self::$name(self.into()) ),*
                }
            }
        }

        gen_value_type!{ gen_math $( $name ),* for Add add, Sub sub, Mul mul, Div div }

        impl TypedWriter<Value> for Out {
            fn write(&mut self, v: Value) -> Result<(), crate::error::Error> {
                match v {
                    $( Value::$name(v) => self.write(v) ),*
                }
            }
        }
    };
}

gen_value_type!(U16 => u16, I16 => i16, I32 => i32, Float => f32);

impl Default for Value {
    fn default() -> Self {
        Self::I16(0)
    }
}

impl Type {
    fn and(&self, rhs: &Self) -> Self {
        match (self, rhs) {
            (_, Type::Float) | (Type::Float, _) => Type::Float,
            (_, Type::I32) | (Type::I32, _) => Type::I32,
            (Type::U16, Type::I16) | (Type::I16, Type::U16) => Type::I32,
            (Type::I16, Type::I16) => Type::I16,
            (Type::U16, Type::U16) => Type::I16,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[enum_dispatch(Register)]
pub enum RegisterType {
    Zero(Zero),
    Static(Static),
    Noise(Noise),

    // Special
    PortId(PortId),

    // Meta
    Cast(Cast),
    Remap(Remap),
    List(List),
}

impl Default for RegisterType {
    fn default() -> Self {
        Self::Zero(Zero)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Zero;
impl Register for Zero {
    fn read(&self, _: &Context) -> Value {
        Value::default()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Static(Value);
impl Register for Static {
    fn read(&self, _: &Context) -> Value {
        self.0
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Noise {
    #[serde(skip)]
    pub noise: Box<OpenSimplex>, // Box beacause it's big
    pub pos: f64,
    pub scale: f64,
}

impl Noise {
    const RANGE: f64 = 0.544;
}
impl Register for Noise {
    fn read(&self, ctx: &Context) -> Value {
        let norm =
            (self.noise.get([self.pos, ctx.time * self.scale]) + Self::RANGE) / (2.0 * Self::RANGE);
        Value::Float(norm as f32)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Cast(Box<RegisterType>, Type);
impl Register for Cast {
    fn read(&self, ctx: &Context) -> Value {
        self.0.read(ctx).cast(self.1)
    }

    fn write(&mut self, val: Value) -> bool {
        self.0.write(val.cast(self.1))
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Remap(pub Box<RegisterType>, pub Range<Value>);
impl Register for Remap {
    fn read(&self, ctx: &Context) -> Value {
        self.0.read(ctx) * (self.1.end - self.1.start) + self.1.start
    }

    fn write(&mut self, _val: Value) -> bool {
        false
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct List(pub Vec<(Value, RegisterType)>);
impl Register for List {
    fn read(&self, ctx: &Context) -> Value {
        self.0
            .iter()
            .map(|(c, v)| *c * v.read(ctx))
            .reduce(|a, b| a + b)
            .unwrap_or_default()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PortId;
impl Register for PortId {
    fn read(&self, ctx: &Context) -> Value {
        Value::U16(ctx.reg_id)
    }
}
