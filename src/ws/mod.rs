use std::time::SystemTime;

use futures::{future::OptionFuture, SinkExt, StreamExt};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use tokio::{
    net::{TcpListener, TcpStream},
    time::{interval, Interval},
};

use crate::{
    curve_generator::RegisterType,
    server::{self, Server, ServerConfig},
};

#[derive(Debug, Clone)]
pub struct WebSocketServer(Server);

/// Message from the client to the server
#[derive(Debug, Deserialize)]
enum MessageIn {
    SetPoll(Option<std::time::Duration>),
    SetRegisterConf {
        reg_id: u16,
        conf: Option<RegisterType>,
    },
}

/// Message sent by the server to the client
#[derive(Debug, Serialize)]
enum MessageOut {
    Configuration(ServerConfig),
    History(SystemTime, server::history::Entry),
    HistoryLagged(u64),
}

#[derive(Debug, Error)]
enum ClientError {
    #[error("io error {0}")]
    Io(#[from] std::io::Error),

    #[error("websocket error {0}")]
    Ws(#[from] async_tungstenite::tungstenite::Error),

    #[error("ser/deserialization error {0}")]
    Serde(#[from] serde_json::Error),
}

impl WebSocketServer {
    pub fn new(srv: Server) -> Self {
        Self(srv)
    }

    pub async fn run(self, socket_addr: impl Into<std::net::SocketAddr>) -> std::io::Result<()> {
        let listener = TcpListener::bind(socket_addr.into()).await?;

        let (mut conf, _) = self.0.watch();
        let mut poll_timer = self.0.config().poll_interval.map(interval);

        loop {
            tokio::select! {
                o = listener.accept() => {
                    match o {
                        Ok((stream, _)) => {
                            tokio::spawn(self.clone().accept_connection(stream));
                        },
                        Err(err) => {
                            println!("[ERROR]: {err}");
                            return Err(err); // When unable to accept new connection, stop websocket server
                        }
                    }
                }

                () = conf.changed() => {
                    let poll_interval = self.0.config().poll_interval;
                    if poll_timer.as_ref().map(Interval::period) != poll_interval {
                        poll_timer = poll_interval.map(interval);
                    }
                }

                Some(_) = OptionFuture::from(poll_timer.as_mut().map(Interval::tick)) => {
                    self.0.send_snapshot()
                }
            }
        }
    }

    async fn accept_connection(self, stream: TcpStream) -> Result<(), ClientError> {
        use async_tungstenite::tungstenite::protocol::Message;
        let (mut conf, mut hist) = self.0.watch();

        let mut ws_stream = async_tungstenite::tokio::accept_async(stream).await?;

        // Send init conf
        ws_stream
            .send(Message::Text(serde_json::to_string(
                &MessageOut::Configuration(self.0.config()),
            )?))
            .await?;

        loop {
            let msg = tokio::select! {
                Some(msg) = ws_stream.next() => {
                    let m = msg?;
                    if m.is_close() {
                        return Ok(())
                    } else if let Ok(m) = m.to_text() {
                        let i = serde_json::from_str::<MessageIn>(m)?;
                        match i {
                            MessageIn::SetPoll(d) => self.0.set_poll_interval(d),
                            MessageIn::SetRegisterConf { reg_id, conf } => self.0.set_register_conf(reg_id, conf),
                        }
                    }

                    None
                }

                h = hist.recv() => {
                    use tokio::sync::broadcast::error::RecvError;
                    Some(match h {
                        Ok((time, hist)) => MessageOut::History(time, hist),
                        Err(RecvError::Lagged(lag)) => MessageOut::HistoryLagged(lag),
                        // Can't be closed, we still have a handle on the server (that has the sender)
                        Err(RecvError::Closed) => unreachable!(),
                    })
                }

                () = conf.changed() => {
                    Some(MessageOut::Configuration(self.0.config()))
                }
            };

            if let Some(msg) = msg {
                ws_stream
                    .send(Message::Text(serde_json::to_string(&msg)?))
                    .await?;
            }
        }
    }
}
